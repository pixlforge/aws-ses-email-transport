<?php

  namespace AwsSesEmailTransport\Mailer\Transport;

  use Cake\Mailer\AbstractTransport;
  use Cake\Mailer\Email;
  use Aws\Ses\SesClient;

  class AwsSesEmailTransport extends AbstractTransport
  {

    public function send(Email $email)
    {

      $client = new SesClient([
        'region' => $this->_config['region'],
        'version' => $this->_config['version'],
      ]);

      $result = $client->sendEmail([
        'Destination' => [
          'BccAddresses' => array_values($email->getBcc()),
          'CcAddresses' => array_values($email->getCc()),
          'ToAddresses' => array_values($email->getTo()),
        ],
        'Message' => [
          'Body' => [
            'Html' => [
              'Charset' => $email->getCharset(),
              'Data' => $email->message(Email::MESSAGE_HTML),
            ],
            'Text' => [
              'Charset' => $email->getCharset(),
              'Data' => $email->message(Email::MESSAGE_TEXT),
            ]
          ],
          'Subject' => [
            'Charset' => $email->getCharset(),
            'Data' => $email->getSubject(),
          ]
        ],
        'Source' => $this->_getSender($email),
      ]);
    }

    private function _getSender(Email $email) {
      $senders = $email->getFrom();
      $from = NULL;
      foreach ($senders as $key => $val)
      {
        $from = $val.' <'.$key.'>';
      }
      return $from;
    }

  }
